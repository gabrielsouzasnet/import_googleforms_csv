package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/caarlos0/env"
	"github.com/gocarina/gocsv"

	"gitcodata.pb.gov.br/import_codata_csv/internal/platform/importcsvgoogle"
	"github.com/go-pg/pg/v10"
)

var (
	arqConfig string
)

func init() {
	flag.StringVar(&arqConfig, "conf", "", "Arquivo de configurações em formato json")
}

func main() {
	flag.Parse()

	conf := NewConfig(arqConfig)
	b, _ := json.Marshal(conf)
	log.Println("config:", string(b))

	db := pg.Connect(&pg.Options{
		Addr:     ":5432",
		User:     conf.UserDB,
		Password: conf.PasswordDB,
		Database: conf.NameDB,
	})
	defer db.Close()

	err := db.Ping(db.Context())
	if err != nil {
		panic(err)
	}

	log.Println("Banco conectado com sucesso!")

	mapSessao, err := importcsvgoogle.CreateMapSessao(db)
	if err != nil {
		panic(err)
	}
	fmt.Println("mapSessao:", mapSessao)
	mapPerguntas, err := importcsvgoogle.CreateMapPerguntas(db)
	if err != nil {
		panic(err)
	}
	fmt.Println("mapPerguntas:", mapPerguntas)
	mapOpcoes, err := importcsvgoogle.CreateMapOpcoes(db)
	if err != nil {
		panic(err)
	}
	fmt.Println("mapOpcoes:", mapOpcoes)

	questionsFile, err := os.OpenFile(conf.FileCSV, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer questionsFile.Close()

	questions := []*importcsvgoogle.Questions{}

	if err := gocsv.UnmarshalFile(questionsFile, &questions); err != nil { // Load clients from file
		panic(err)
	}

	log.Println("Quantidade de lihas: ", len(questions))
	for _, q := range questions {
		layout := "1/02/2006 15:04:05"
		t, err := time.Parse(layout, q.Timestamp)
		if err != nil {
			panic(err)
		}

		dbTx, err := db.Begin()
		if err != nil {
			panic(err)
			return
		}

		c := &importcsvgoogle.Colaborador{
			MomentoCadastro: t,
			Nome:            q.Nome,
			Email:           q.Email,
			Setor:           q.Setor,
		}

		err = dbTx.Insert(c)
		if err != nil {
			panic(err)
		}

		indexField := 4
		err = importcsvgoogle.InsertRespostas(dbTx, c, q, indexField, mapOpcoes, mapPerguntas)
		if err != nil {
			dbTx.Rollback()
			panic(err)
			return
		}

		dbTx.Commit()
	}

}

//Config ...
type Config struct {
	FileCSV    string `json:"fileCSV" env:"FILE_CSV"`
	UserDB     string `json:"userDB" env:"USER_DB"`
	NameDB     string `json:"nameDB" env:"NAME_DB"`
	PasswordDB string `json:"passwordDB" env:"PASSWORD_DB"`
}

//NewConfig ...
func NewConfig(file string) *Config {
	var erro error

	conf := &Config{}

	if file != "" {
		bufConf, err := ioutil.ReadFile(file)
		if err == nil {
			erro = json.Unmarshal(bufConf, conf)
			if erro != nil {
				log.Println(erro)
			}
		}
	}

	//variaveis de ambiente sobrescrevem informacoes do json
	if erro = env.Parse(conf); erro != nil {
		log.Println(erro)
	}

	return conf
}
