PROJECT = import_codata_csv

build:
	go build -o $(PROJECT) cmd/import_csv_google/main.go

run:
	./$(PROJECT) -conf conf.json

go:  
	build run

build-linux:
	env GOOS=linux go build -o $(PROJECT) cmd/import_csv_google/main.go