package importcsvgoogle

import "time"

//Colaborador ...
type Colaborador struct {
	tableName       struct{}  `pg:"colaborador"`
	ID              int       `pg:"id"`
	Nome            string    `pg:"nome"`
	Email           string    `pg:"email"`
	Setor           string    `pg:"setor"`
	MomentoCadastro time.Time `pg:"momento_cadastro"`
}

//ColaboradoResposta ...
type ColaboradoResposta struct {
	tableName              struct{} `pg:"colaboradoresposta"`
	ID                     int      `pg:"id"`
	IDQuestionarioPergunta int      `pg:"idquestionariopergunta"`
	IDQuestionarioOpcao    int      `pg:"idquestionariopcao"`
	IDColaborador          int      `pg:"idcolaborador"`
}

//QuetionariOpcoes ...
type QuetionariOpcoes struct {
	tableName struct{} `pg:"questionariopcoes"`
	ID        int      `pg:"id"`
	Nome      string   `pg:"nome"`
}

//QuestionarioPergunta ...
type QuestionarioPergunta struct {
	tableName            struct{} `pg:"questionariopergunta"`
	ID                   int      `pg:"id"`
	Nome                 string   `pg:"nome"`
	IDQuestionarioSessao int      `pg:"idquestionariosessao"`
}

//QuestionarioSessao ...
type QuestionarioSessao struct {
	tableName struct{} `pg:"questionariosessao"`
	ID        int      `pg:"id"`
	Nome      string   `pg:"nome"`
}
