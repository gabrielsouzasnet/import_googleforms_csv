package importcsvgoogle

import (
	"fmt"
	"reflect"

	"github.com/go-pg/pg/v10"
)

//CreateMapSessao ...
func CreateMapSessao(db *pg.DB) (map[string]int,error) {
	// Select all users.

	var qsess []QuestionarioSessao

	err := db.Model(&qsess).Select()
	if err != nil {
		return nil,err
	}

	mapSessao := make(map[string]int)

	for _, qs := range qsess {
		mapSessao[qs.Nome] = qs.ID
	}

	return mapSessao,nil
}

//CreateMapPerguntas ...
func CreateMapPerguntas(db *pg.DB) (map[string]int,error) {
	var qper []QuestionarioPergunta

	err := db.Model(&qper).Select()
	if err != nil {
		return nil,err
	}

	mapaItens := make(map[string]int)

	for _, qp := range qper {
		mapaItens[qp.Nome] = qp.ID
	}

	return mapaItens,nil
}

//CreateMapOpcoes ...
func CreateMapOpcoes(db *pg.DB) (map[string]int,error) {
	var qop []QuetionariOpcoes

	err := db.Model(&qop).Select()
	if err != nil {
		return nil,err
	}

	mapaItens := make(map[string]int)

	for _, qp := range qop {
		mapaItens[qp.Nome] = qp.ID
	}

	return mapaItens,nil
}

//InsertRespostas ...
func InsertRespostas(db *pg.Tx,c *Colaborador, q *Questions,indexInit int,mapOpcoes,mapPerguntas map[string]int) error {
	st := reflect.TypeOf(*q)
	val := reflect.ValueOf(q).Elem()


	for i := indexInit; i < val.NumField(); i++ {
		//Aqui pega a tag neste caso a pergunta
		pergunta := st.Field(i).Tag.Get("csv")

		//Pega valor do campo no caso a resposta
		valueField := val.Field(i)
		f := valueField.Interface()
		val := reflect.ValueOf(f)
		opcao := val.String()

		fmt.Println("Cadastrar resposta para pergunta:",pergunta," opcao{ id:", mapOpcoes[opcao], " reposta: ",opcao,"stringvazia: ",opcao=="","}, pergunta{ id:",mapPerguntas[pergunta]," resposta:",pergunta)


		if opcao == "" {
			opcao = "Nao Informado"
		}

		idOpcao := mapOpcoes[opcao]

		if idOpcao == 0 {
			qo := &QuetionariOpcoes{
				Nome: opcao,
			}

			err := db.Insert(qo)
			if err != nil {
				return err
			}

			idOpcao = qo.ID
		}

		cr := &ColaboradoResposta{
			IDColaborador: c.ID,
			IDQuestionarioOpcao: idOpcao,
			IDQuestionarioPergunta: mapPerguntas[pergunta],
		}

		err := db.Insert(cr)
		if err != nil {
			return err
		}
	}

	return nil
}