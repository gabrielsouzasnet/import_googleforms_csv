package importcsvgoogle

//Questions ...
type Questions struct {
	Timestamp        string `csv:"Timestamp"`
	Nome             string `csv:"Nome"`
	Email            string `csv:"E-mail"`
	Setor            string `csv:"Setor"`
	C                string `csv:"C"`
	Cmais            string `csv:"C ++"`
	CSharp           string `csv:"C #"`
	Java             string `csv:"JAVA"`
	Python           string `csv:"PYTHON"`
	Golang           string `csv:"Golang"`
	Rust             string `csv:"RUST"`
	Ruby             string `csv:"Ruby"`
	JavaScript       string `csv:"JAVASCRIPT"`
	Php              string `csv:"PHP"`
	JavaMobile       string `csv:"JAVA PARA MOBILE"`
	Node             string `csv:"NODE"`
	ShellScript      string `csv:"SHELL SCRIPT"`
	SQL              string `csv:"SQL"`
	PlPsql           string `csv:"PL/PSQL"`
	HTML             string `csv:"HTML"`
	CSS              string `csv:"CSS"`
	Thymeleaf        string `csv:"Thymeleaf"`
	Django           string `csv:"Django"`
	RubyOnRails      string `csv:"Ruby on Rails"`
	SpringBoot       string `csv:"SpringBoot"`
	Angular          string `csv:"Angular"`
	VueJS            string `csv:"VueJS"`
	ReactJS          string `csv:"ReactJS"`
	SASS             string `csv:"SASS"`
	JpaHibernate     string `csv:"JPA/Hibernate"`
	EntityFramework  string `csv:"EntityFramework"`
	SQLAlchemy       string `csv:"SqlAlchemy"`
	DjangoOrm        string `csv:"DjangoOrm"`
	ActiveRecord     string `csv:"ActiveRecord"`
	Gorm             string `csv:"Gorm"`
	Sequelize        string `csv:"Sequelize"`
	AspNetCore       string `csv:"AspNet Core"`
	Ansible          string `csv:"Ansible"`
	Terraform        string `csv:"Terraform"`
	Postgres         string `csv:"Postgres"`
	Oracle           string `csv:"Oracle"`
	Mysql            string `csv:"Mysql"`
	MariaDB          string `csv:"MariaDB"`
	SQLServer        string `csv:"SqlServer"`
	MongoDB          string `csv:"MongoDB"`
	Redis            string `csv:"Redis"`
	Elasticsearch    string `csv:"Elasticsearch"`
	Neo4j            string `csv:"Neo4j"`
	Cassandra        string `csv:"Cassandra"`
	InfluxDB         string `csv:"InfluxDB"`
	Sqlite           string `csv:"Sqlite"`
	Consul           string `csv:"Consul"`
	Etcd             string `csv:"Etcd"`
	Zookeeper        string `csv:"Zookeeper"`
	Kafka            string `csv:"Kafka"`
	RabbitMQ         string `csv:"RabbitMQ"`
	ActiveMQ         string `csv:"ActiveMQ"`
	ZeroMQ           string `csv:"ZeroMQ"`
	Docker           string `csv:"Docker"`
	Kubernetes       string `csv:"Kubernetes"`
	Rancher          string `csv:"Rancher"`
	Nomad            string `csv:"Nomad"`
	DockerSwarm      string `csv:"Docker Swarm"`
	Openshift        string `csv:"Openshift"`
	Wordpress        string `csv:"Wordpress"`
	Plone            string `csv:"Plone"`
	Git              string `csv:"Git"`
	SVN              string `csv:"SVN"`
	Nginx            string `csv:"Nginx"`
	Apache           string `csv:"Apache"`
	Traefik          string `csv:"Traefik"`
	Haproxy          string `csv:"Haproxy"`
	Linguagem        string `csv:"Linguagem"`
	Framework        string `csv:"Framework"`
	BancoDeDados     string `csv:"Banco de Dados"`
	Infraestrutura   string `csv:"infraestrutura"`
	CMS              string `csv:"CMS"`
	ControleDeVersao string `csv:"Controle de Versão"`
	WebServer        string `csv:"Web Server"`
	Sugestao         string `csv:"Teria alguma sugestão de ferramenta para melhorar nosso trabalho na CODATA?"`
	Outros           string `csv:"Outros conhecimentos"`
}
