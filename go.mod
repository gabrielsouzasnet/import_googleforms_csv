module gitcodata.pb.gov.br/import_codata_csv

go 1.13

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-pg/pg/v10 v10.0.0-beta.1
	github.com/gocarina/gocsv v0.0.0-20200330101823-46266ca37bd3
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.6.0
)
